#!/bin/bash

#Install oh-my-zsh
if ! [ -f $HOME/.zshrc ]; then
	echo "download zshrc file"
	cp zshrc $HOME/.zshrc

	echo "Installing Oh-My-ZSH"
	sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
	cp melston.zsh-theme $HOME/.oh-my-zsh/themes/melston.zsh-theme
	# open Dracula.terminal
else
	echo "Oh-My-ZSH is installed"
fi


# command line tool directory
cmd_line_tools_dir="/Library/Developer/CommandLineTools"

# check command line tools installed
if [ -d $cmd_line_tools_dir ]; then
    # install xcode command line tools
    echo "Xcode CommandLineTools already installed!"
    # install homebrew
	if brew -v > /dev/null; then
		echo "homebrew installed, ready to rock"
	else
	  echo "installing homebrew"
	  /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
	fi
    
    . $HOME/.zshrc
    # install dependencies
    brew install nvm python git git-lfs > /dev/null
    nvm install --lts > /dev/null
    nvm alias default lts/* > /dev/null
    nvm use default
	npm install -g cordova@7.1.0 json-diff qrcode redoc-cli svgo yaml2json

    # install homebrew formula
	input="formulae.txt"
	while IFS= read -r line
		do
		if brew ls --versions $line > /dev/null; then
		  echo "${line} is installed"
		  # The package is installed
		else
		  echo "INSTALLING ${line}"
		  if $line == 'ffmpeg'; then
		  	brew install $line $(brew options ffmpeg | grep -vE '\s' | grep -- '--with-' | tr '\n' ' ')
		  else
		  	brew install $line
		  fi
		fi
	done < "$input"

	input="casks.txt"
	while IFS= read -r line
		do
		if brew cask ls --versions $line > /dev/null; then
		  echo "${line} is installed"
		  # The package is installed
		else
		  echo "INSTALLING ${line}"
		  brew cask install $line
		fi
	done < "$input"    
	echo "Install Finished!"
else
    echo "install command line tools after install it, please reexcute this script"
    echo "Install xcode command line tools"
    xcode-select --install
fi