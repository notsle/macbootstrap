# Essential installs and setups for Mac

1. Install Oh-My-ZSH `sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"`
2. Set Dracula.terminal as default terminal theme
3. Configure ZSH `cp ./zshrc ~/.zshrc`
4. Install Homebrew `/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`


